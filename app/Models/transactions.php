<?php

namespace App\Models;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;
class transaction extends Model
{
    use HasFactory;
    /**
     * fillable
     * @var array
     */
    protected $fillable =[
        'cashier_id','costumer_id','invoice','cash','change','discount','grand_total','table_total'
    ];

    /**
     * details
     * 
     * @return void
     */
    public function details()
    {
        return $this->HasMany(transaction_details::class);
    }

    /**
     * customers
     * 
     * @return void
     */
    public function costumer()
    {
        return $this->BelongTo(customers::class);
    }
    /**
     * cashier
     * 
     * @return void
     */
    public function cashier()
    {
        return $this->BelongTo(User::class,'cashier_id');
    }
    /**
     * profit
     * 
     * @return void
     */
    public function profit()
    {
        return $this->HasMany(profits::class);
    }
    /**
     * createdAt
     * 
     * @return attribute
     */
    protected function createdAt(): Attribute
    {
        return Attribute::make(
            get: fn ($value) => carbon::parse($value)->format('d-M-Y H:i:s'),
        );
    }
}
